package com.LuizGustavoPeruchideOliveira.exerccio1

import android.content.Context
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.core.content.ContextCompat
import kotlin.math.roundToInt

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val altura = findViewById<EditText>(R.id.altura)
        val peso = findViewById<EditText>(R.id.peso)
        val resultado = findViewById<TextView>(R.id.resultado)
        val preferences = getSharedPreferences("fodasse", Context.MODE_PRIVATE) ?: return
        val texto = preferences.getString("IMC",null)

        if(texto != null){
            resultado.text = texto.toString()
        }


        findViewById<Button>(R.id.calcular).setOnClickListener {


            val trueAltura = altura.text.toString().trim()
            val truePeso = peso.text.toString().trim()
            if(altura.length() == 0 || peso.length() == 0){
                val texto = preferences.getString("IMC",null)
                resultado.text = texto.toString()
            } else {
                if(truePeso.toInt() == 0 || trueAltura.toInt() == 0){
                    resultado.text = "Não divide por 0 mané!"
                    resultado.setTextColor(Color.parseColor("#FF1000"))
                    return@setOnClickListener
                }
                val dividido = truePeso.toFloat() / (trueAltura.toFloat()/10)
                val end = (dividido*dividido*100).roundToInt() / 100.00

                if(end<18.5) {
                    resultado.text =
                        "Você está abaixo do peso normal. \nSeu IMC é de: ${end}"
                    resultado.setTextColor(Color.parseColor("#FF0000"))
                }else if (end>=18.5 && end<25) {
                    resultado.text = "Você tem peso normal. \nSeu IMC é de: ${end}"
                    resultado.setTextColor(Color.parseColor("#008000"))
                }else if (end>=25 && end<30) {
                    resultado.text = "Você tem excesso de peso. \nSeu IMC é de: ${end}"
                    resultado.setTextColor(Color.parseColor("#FFA500"))
                }else if (end>=30 && end<35) {
                    resultado.text = "Você tem obesidade classe I. \nSeu IMC é de: ${end}"
                    resultado.setTextColor(Color.parseColor("#FF8C00"))
                }else if (end>=35 && end<40) {
                    resultado.text = "Você tem obesidade classe II. \nSeu IMC é de: ${end}"
                    resultado.setTextColor(Color.parseColor("#FF4500"))
                }else {
                    resultado.text = "Você tem obesidade classe III. \nSeu IMC é de: ${end}"
                    resultado.setTextColor(Color.parseColor("#FF0000"))
                }
                val texto = resultado.text
                with (preferences.edit()) {
                    putString("IMC", texto.toString())
                    commit()
                }
            } }
    }
}
